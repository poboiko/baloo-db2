#include "positiondb.h"
#include "enginedebug.h"
#include "positioncodec.h"

using namespace Baloo;

PositionDB::PositionDB(MDB_dbi dbi, MDB_txn* txn)
    : m_txn(txn)
    , m_dbi(dbi)
{
}

MDB_dbi PositionDB::create(MDB_txn *txn)
{
    MDB_dbi dbi = 0;
    int rc = mdb_dbi_open(txn, "positiondb", MDB_CREATE | MDB_INTEGERKEY, &dbi);
    if (rc) {
        qCWarning(ENGINE) << "PositionDB::create" << mdb_strerror(rc);
        return 0;
    }
    return dbi;
}

MDB_dbi PositionDB::open(MDB_txn *txn)
{
    MDB_dbi dbi = 0;
    int rc = mdb_dbi_open(txn, "positiondb", MDB_INTEGERKEY, &dbi);
    if (rc) {
        qCWarning(ENGINE) << "PositionDB::open" << mdb_strerror(rc);
        return 0;
    }

    return dbi;
}

unsigned char PositionDB::probe(quint64 hash)
{
    unsigned char delta = 0;
    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_dbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PositionDB::probe" << mdb_strerror(rc);
        return MAX_DELTA;
    }
    MDB_val key, val{0, nullptr};
    key.mv_size = sizeof(hash);
    key.mv_data = &hash;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET_RANGE);
    if (rc) {
        mdb_cursor_close(cursor);
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "PositionDB::probe" << mdb_strerror(rc);
            return MAX_DELTA;
        } else {
            return 0;
        }
    }
    quint64 newhash = *reinterpret_cast<quint64*>(key.mv_data);
    while (delta < MAX_DELTA && newhash == hash + delta) {
        delta++;
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT);
        if (rc == MDB_NOTFOUND)
            break;
        if (rc != 0) {
            qCWarning(ENGINE) << "PositionDB::probe" << mdb_strerror(rc);
            mdb_cursor_close(cursor);
            return 0xFF;
        }
        newhash = *reinterpret_cast<quint64*>(key.mv_data);
    }
    mdb_cursor_close(cursor);
    return delta;
}

int PositionDB::put(quint64 hash, const QByteArray& data)
{
    MDB_val key, val;
    key.mv_size = sizeof(hash);
    key.mv_data = &hash;
    val.mv_size = data.size();
    val.mv_data = reinterpret_cast<void*>(const_cast<char*>(data.data()));
    return mdb_put(m_txn, m_dbi, &key, &val, 0);
}

int PositionDB::del(quint64 hash)
{
    MDB_val key, val;
    key.mv_size = sizeof(hash);
    key.mv_data = &hash;
    val.mv_size = 0;
    val.mv_data = nullptr;
    return mdb_del(m_txn, m_dbi, &key, &val);
}

QVector<quint32> PositionDB::get(quint64 hash)
{
    MDB_val key, val;
    key.mv_size = sizeof(hash);
    key.mv_data = &hash;
    val.mv_size = 0;
    val.mv_data = nullptr;
    int rc = mdb_get(m_txn, m_dbi, &key, &val);
    if (rc) {
        qCWarning(ENGINE) << "PositionDB::get" << hash << mdb_strerror(rc);
        return {};
    }
    QByteArray data = QByteArray::fromRawData(reinterpret_cast<const char*>(val.mv_data), val.mv_size);
    return PositionCodec::decode(data);
}

uint PositionDB::count(quint64 hash)
{
    MDB_val key, val;
    key.mv_size = sizeof(hash);
    key.mv_data = &hash;
    val.mv_size = 0;
    val.mv_data = nullptr;
    int rc = mdb_get(m_txn, m_dbi, &key, &val);
    if (rc) {
        qCWarning(ENGINE) << "PositionDB::get" << hash << mdb_strerror(rc);
        return 0;
    }
    QByteArray data = QByteArray::fromRawData(reinterpret_cast<const char*>(val.mv_data), val.mv_size);
    return PositionCodec::size(data);
}

QMap<quint64,QVector<quint32>> PositionDB::toTestMap()
{
    QMap<quint64, QVector<quint32>> result;
    MDB_val key{0, nullptr}, val{0, nullptr};

    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_dbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PositionDB::toTestMap" << mdb_strerror(rc);
        return result;
    }
    rc = mdb_cursor_get(cursor, &key, &val, MDB_FIRST);
    while (rc == 0) {
        quint64 hash = *reinterpret_cast<quint64*>(key.mv_data);
        QByteArray data = QByteArray::fromRawData(reinterpret_cast<const char*>(val.mv_data), val.mv_size);
        result[hash] = PositionCodec::decode(data);
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT);
    }
    if (rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "PositionDB::toTestMap" << mdb_strerror(rc);
    }

    mdb_cursor_close(cursor);
    return result;
}
