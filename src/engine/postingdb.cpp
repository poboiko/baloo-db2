#include "postingdb.h"
#include "positiondb.h"
#include "enginedebug.h"
#include "positioncodec.h"
#include "vectorpostingiterator.h"
#include "orpostingiterator.h"
#include <QCryptographicHash>
#include <QtEndian>
#include <cmath>

using namespace Baloo;

namespace {

// Helper functions
static quint64 postingHash(const QByteArray &term, quint64 docId)
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(term);
    hash.addData(reinterpret_cast<const char*>(&docId), sizeof(docId));
    QByteArray result = hash.result();
    return qFromBigEndian<quint64>(result.data()) ^ qFromBigEndian<quint64>(result.data() + 8);
}

static QVector<quint32> fetchPositions(MDB_txn* txn, MDB_dbi positionDbi, const MDB_val& key, const MDB_val& val) {
    if (val.mv_size == sizeof(quint64) + sizeof(char)) {
        PositionDB positionDb(positionDbi, txn);
        QByteArray term = QByteArray::fromRawData(reinterpret_cast<char*>(key.mv_data), key.mv_size);
        quint64 docId = *reinterpret_cast<quint64*>(val.mv_data);
        quint64 hash = postingHash(term, docId);
        unsigned char delta = *(reinterpret_cast<unsigned char*>(val.mv_data) + sizeof(quint64));
        return positionDb.get(hash + delta);
    } else if (val.mv_size > sizeof(quint64)) {
        QByteArray posData = QByteArray::fromRawData(reinterpret_cast<const char*>(val.mv_data) + sizeof(quint64), val.mv_size - sizeof(quint64));
        return PositionCodec::decode(posData);
    } else {
        return {};
    }
}

inline int MDBCompare(const MDB_val* a, const MDB_val* b) {
    quint64 vala = *reinterpret_cast<quint64*>(a->mv_data);
    quint64 valb = *reinterpret_cast<quint64*>(b->mv_data);
    return (vala > valb) ? 1 : ((vala == valb) ? 0 : -1);
}

}

PostingDB::PostingDB(MDB_dbi postingDbi, MDB_dbi positionDbi, MDB_txn* txn)
    : m_txn(txn)
    , m_postingDbi(postingDbi)
    , m_positionDbi(positionDbi)
{
    MDB_env* env = mdb_txn_env(txn);
    m_maxkeysize = mdb_env_get_maxkeysize(env);
}

MDB_dbi PostingDB::create(MDB_txn *txn)
{
    MDB_dbi dbi = 0;
    int rc = mdb_dbi_open(txn, "postingdb", MDB_CREATE | MDB_DUPSORT, &dbi);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::create" << mdb_strerror(rc);
        return 0;
    }
    rc = mdb_set_dupsort(txn, dbi, MDBCompare);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::create" << mdb_strerror(rc);
        return 0;
    }
    return dbi;
}

MDB_dbi PostingDB::open(MDB_txn *txn)
{
    MDB_dbi dbi = 0;
    int rc = mdb_dbi_open(txn, "postingdb", MDB_DUPSORT, &dbi);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::open" << mdb_strerror(rc);
        return 0;
    }
    rc = mdb_set_dupsort(txn, dbi, MDBCompare);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::open" << mdb_strerror(rc);
        return 0;
    }
    return dbi;
}

void PostingDB::put(const QByteArray &term, quint64 docId, const PositionsList& positions)
{
    Q_ASSERT(!term.isEmpty());
    Q_ASSERT(docId > 0);

    PositionDB positionDb(m_positionDbi, m_txn);
    // Trying to find (term, docId) pair inside PostingDB
    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc != 0) {
        qCWarning(ENGINE) << "PostingDB::put" << mdb_strerror(rc);
        return;
    }
    MDB_val key, val;
    key.mv_size = term.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(term.data()));
    val.mv_size = sizeof(docId);
    val.mv_data = &docId;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_GET_BOTH);
    if (rc != 0 && rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "PostingDB::put" << mdb_strerror(rc);
        mdb_cursor_close(cursor);
        return;
    }

    bool found = (rc == 0);

    quint64 hash = postingHash(term, docId);
    unsigned char olddelta = PositionDB::MAX_DELTA, delta = PositionDB::MAX_DELTA;

    if (found && val.mv_size == sizeof(quint64) + sizeof(unsigned char))
        olddelta = *(reinterpret_cast<unsigned char*>(val.mv_data) + sizeof(quint64));

    // Preparing...
    key.mv_size = term.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(term.data()));

    QByteArray valdata;
    if (positions.size() > 0) {
        QByteArray posData = PositionCodec::encode(positions);
        // There are positions -> trying to fit it inside PostingDB alone
        if (sizeof(quint64) + posData.size() <= m_maxkeysize) {
            valdata = QByteArray(reinterpret_cast<char*>(&docId), sizeof(quint64)) + posData;
            val.mv_size = valdata.size();
            val.mv_data = reinterpret_cast<void*>(const_cast<char*>(valdata.data()));
        } else {
            // Cannot fit -> we have to put it inside PositionDB
            if (olddelta == PositionDB::MAX_DELTA) {
                // If there was no link present before -> trying to create a new one
                delta = positionDb.probe(hash);
            } else {
                delta = olddelta;
            }
            if (delta < PositionDB::MAX_DELTA) {
                // We have found a position to put it
                rc = positionDb.put(hash + delta, posData);
                if (rc) {
                    // But failed to put
                    delta = PositionDB::MAX_DELTA;
                }
            }
            // Data successfully (or not) stored inside PositionDB
            if (delta < PositionDB::MAX_DELTA) {
                // Packing docId + delta
                valdata.reserve(sizeof(quint64) + sizeof(char));
                valdata.append(reinterpret_cast<char*>(&docId), sizeof(docId));
                valdata.append(delta);
                val.mv_size = valdata.size();
                val.mv_data = reinterpret_cast<void*>(const_cast<char*>(valdata.data()));
            } else {
                // Failed to put it into PositionDB -> just put docId
                val.mv_size = sizeof(quint64);
                val.mv_data = &docId;
            }
        }
    } else {
        // No position data -> just put docId
        val.mv_size = sizeof(quint64);
        val.mv_data = &docId;
    }

    if (delta == PositionDB::MAX_DELTA && olddelta < PositionDB::MAX_DELTA) {
        // Positions were inside PositionDB, and not anymore -> remove old junk
        positionDb.del(hash + olddelta);
    }

    // If found existing entry -> replace it, otherwise --- add new
    // Seems like MDB_CURRENT doesn't work with MDB_DUPSORT (?)
    //rc = mdb_cursor_put(cursor, &key, &val, (foundid == docId) ? MDB_CURRENT : 0);
    if (found) {
        rc = mdb_cursor_del(cursor, 0);
        if (rc) {
            qCWarning(ENGINE) << "PostingDB::put" << mdb_strerror(rc);
        }
    }
    rc = mdb_cursor_put(cursor, &key, &val, 0);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::put" << mdb_strerror(rc);
    }
    mdb_cursor_close(cursor);
}

QVector<quint64> PostingDB::get(const QByteArray &term)
{
    Q_ASSERT(!term.isEmpty());

    QVector<quint64> result;

    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::get" << mdb_strerror(rc);
        return result;
    }

    MDB_val key, val{0, nullptr};
    key.mv_size = term.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(term.data()));

    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET);

    while (rc == 0) {
        quint64 id = *reinterpret_cast<quint64*>(val.mv_data);
        result.append(id);
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT_DUP);
    }

    if (rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "PostingDB::get" << mdb_strerror(rc);
    }

    mdb_cursor_close(cursor);
    return result;
}

PositionsList PostingDB::get(const QByteArray& term, quint64 docId)
{
    Q_ASSERT(!term.isEmpty());

    PositionsList result;

    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc != 0) {
        qCWarning(ENGINE) << "PostingDB::get" << mdb_strerror(rc);
        return result;
    }
    MDB_val key, val;
    key.mv_size = term.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(term.data()));
    val.mv_size = sizeof(docId);
    val.mv_data = &docId;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_GET_BOTH);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "PostingDB::get" << mdb_strerror(rc);
        }
        mdb_cursor_close(cursor);
        return result;
    }
    result = fetchPositions(m_txn, m_positionDbi, key, val);
    mdb_cursor_close(cursor);
    return result;
}

void PostingDB::del(const QByteArray &term, quint64 docId)
{
    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc != 0) {
        qCWarning(ENGINE) << "PostingDB::get" << mdb_strerror(rc);
        return;
    }
    MDB_val key, val;
    key.mv_size = term.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(term.data()));
    val.mv_size = sizeof(docId);
    val.mv_data = &docId;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_GET_BOTH);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "PostingDB::del" << mdb_strerror(rc);
        }
        mdb_cursor_close(cursor);
        return;
    }
    if (val.mv_size == sizeof(quint64) + sizeof(char)) {
        PositionDB positionDb(m_positionDbi, m_txn);
        unsigned char delta = *(reinterpret_cast<char*>(val.mv_data) + sizeof(quint64));
        quint64 hash = postingHash(term, docId);
        positionDb.del(hash + delta);
    }
    rc = mdb_cursor_del(cursor, 0);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::del" << mdb_strerror(rc);
    }
    mdb_cursor_close(cursor);
    return;
}

uint PostingDB::count(const QByteArray& term)
{
    Q_ASSERT(!term.isEmpty());
    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc != 0) {
        qCWarning(ENGINE) << "PostingDB::count" << term << mdb_strerror(rc);
        return 0;
    }
    MDB_val key, val{0, nullptr};
    key.mv_size = term.size();
    key.mv_data = const_cast<char*>(term.data());
    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET);
    size_t N = 0;
    if (rc == 0) {
        rc = mdb_cursor_count(cursor, &N);
    }
    if (rc != 0 && rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "PostingDB::count" << term << mdb_strerror(rc);

    }
    return N;
}

uint PostingDB::count(const QByteArray& term, quint64 docId)
{
    Q_ASSERT(!term.isEmpty());
    Q_ASSERT(docId > 0);

    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc != 0) {
        qCWarning(ENGINE) << "PostingDB::count" << term << docId << mdb_strerror(rc);
        return 0;
    }

    MDB_val key, val;
    key.mv_size = term.size();
    key.mv_data = const_cast<char*>(term.data());
    val.mv_size = sizeof(quint64);
    val.mv_data = &docId;

    rc = mdb_cursor_get(cursor, &key, &val, MDB_GET_BOTH);
    if (rc != 0) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "PostingDB::count" << term << docId << mdb_strerror(rc);
        }
        mdb_cursor_close(cursor);
        return 0;
    }
    uint result;
    if (val.mv_size == sizeof(quint64)) {
        // No position data - but there is definitely 1 encounter
        result = 1;
    } else if (val.mv_size == sizeof(quint64) + sizeof(char)) {
        // Position data is inside PositionDB
        PositionDB posDb(m_positionDbi, m_txn);
        quint64 hash = postingHash(term, docId);
        unsigned char delta = *(reinterpret_cast<unsigned char*>(val.mv_data) + sizeof(quint64));
        result = posDb.count(hash + delta);
    } else {
        QByteArray posData = QByteArray::fromRawData(reinterpret_cast<const char*>(val.mv_data) + sizeof(quint64), val.mv_size - sizeof(quint64));
        result = PositionCodec::size(posData);
    }

    return result;
}

QVector<QByteArray> PostingDB::fetchTermsStartingWith(const QByteArray &prefix)
{
    Q_ASSERT(!prefix.isEmpty());

    QVector<QByteArray> terms;

    MDB_val key;
    key.mv_size = prefix.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(prefix.constData()));

    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::fetchTermsStartingWith" << mdb_strerror(rc);
        return terms;
    }

    rc = mdb_cursor_get(cursor, &key, nullptr, MDB_SET_RANGE);
    while (rc == 0) {
        const QByteArray arr(reinterpret_cast<char*>(key.mv_data), key.mv_size);
        if (!arr.startsWith(prefix)) {
            break;
        }
        terms << arr;
        rc = mdb_cursor_get(cursor, &key, nullptr, MDB_NEXT_NODUP);
    }
    if (rc != MDB_NOTFOUND) {
        qCDebug(ENGINE) << "PostingDB::fetchTermsStartingWith" << mdb_strerror(rc);
    }
    mdb_cursor_close(cursor);
    return terms;
}

QMap<QByteArray, PostingMap> PostingDB::toTestMap()
{
    PositionDB positionDb(m_positionDbi, m_txn);
    auto positionMap = positionDb.toTestMap();
    int fetchedPositionEntries = 0;

    QMap<QByteArray, PostingMap> result;
    MDB_val key{0, nullptr}, val{0, nullptr};

    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::toTestMap" << mdb_strerror(rc);
        return result;
    }
    rc = mdb_cursor_get(cursor, &key, &val, MDB_FIRST);
    while (rc == 0) {
        QByteArray curTerm = QByteArray::fromRawData(reinterpret_cast<const char*>(key.mv_data), key.mv_size);
        PostingMap curPosting;
        while (rc == 0) {
            quint64 docId = *reinterpret_cast<quint64*>(val.mv_data);
            // Fetching
            if (val.mv_size == sizeof(quint64) + sizeof(char)) {
                QByteArray term = QByteArray::fromRawData(reinterpret_cast<char*>(key.mv_data), key.mv_size);
                quint64 docId = *reinterpret_cast<quint64*>(val.mv_data);
                quint64 hash = postingHash(term, docId);
                unsigned char delta = *(reinterpret_cast<unsigned char*>(val.mv_data) + sizeof(quint64));
                fetchedPositionEntries++;
                curPosting[docId] = positionMap[hash + delta];
            } else {
                QByteArray posData = QByteArray::fromRawData(reinterpret_cast<const char*>(val.mv_data) + sizeof(quint64), val.mv_size - sizeof(quint64));
                curPosting[docId] = PositionCodec::decode(posData);
            }
            rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT_DUP);
        }
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "PostingDB::toTestMap" << mdb_strerror(rc);
            break;
        }
        result[curTerm] = curPosting;
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT);
    }
    if (rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "PostingDB::toTestMap" << mdb_strerror(rc);
    }
    if (fetchedPositionEntries != positionMap.size()) {
        qCWarning(ENGINE) << "PostingDB::toTestMap" << "Some unused junk found inside PositionDB!";
    }
    mdb_cursor_close(cursor);
    return result;
}

// Iterators
namespace Baloo {
/**
 * Iterates over all documents for particular term
 * Also provides position info
 *
 * NOTE: takes ownership of the iterator!
 */
class DBTermIterator : public PostingIterator
{
public:
    /**
     * NOTE: takes ownership of @p cursor
     */
    DBTermIterator(MDB_dbi positionDbi, MDB_cursor* cursor, const QByteArray &term, MDB_val &&val);
    ~DBTermIterator() override;
    quint64 docId() const override;
    quint64 next() override;
    quint64 skipTo(quint64 docId) override;
    QVector<quint32> positions() override;
private:
    MDB_dbi m_positionDbi;
    MDB_cursor* m_cursor;
    QByteArray m_term;
    MDB_val m_key, m_val;
    QVector<quint32> m_positions;
    bool m_first, m_done, m_positionsFetched;
};

}

DBTermIterator::DBTermIterator(MDB_dbi positionDbi, MDB_cursor* cursor, const QByteArray& term, MDB_val&& val)
    : m_positionDbi(positionDbi)
    , m_cursor(cursor)
    , m_term(term)
    , m_val(val)
    , m_first(true), m_done(false), m_positionsFetched(false)
{
    m_key.mv_data = const_cast<char*>(term.data());
    m_key.mv_size = term.size();
}

DBTermIterator::~DBTermIterator()
{
    mdb_cursor_close(m_cursor);
}

quint64 DBTermIterator::docId() const
{
    if (m_first || m_done) {
        return 0;
    }
    return *reinterpret_cast<quint64*>(m_val.mv_data);
}

quint64 DBTermIterator::next()
{
    if (m_first) {
        m_first = false;
        return docId();
    }
    if (m_done) {
        return 0;
    }
    m_positionsFetched = false;
    m_positions.clear();
    int rc = mdb_cursor_get(m_cursor, &m_key, &m_val, MDB_NEXT_DUP);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "DBTermIterator::next" << mdb_strerror(rc);
        }
        m_done = true;
    }
    return docId();
}

quint64 DBTermIterator::skipTo(quint64 id) {
    if (id <= docId() || m_done) {
        return docId();
    }
    m_first = false;
    m_done = false;
    m_positionsFetched = false;
    m_positions.clear();

    m_val.mv_size = sizeof(id);
    m_val.mv_data = &id;
    int rc = mdb_cursor_get(m_cursor, &m_key, &m_val, MDB_GET_BOTH_RANGE);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "DBTermIterator::skipTo" << mdb_strerror(rc);
        }
        m_done = true;
    }
    return docId();
}

QVector<quint32> DBTermIterator::positions()
{
    if (m_first || m_done) {
        return {};
    }
    if (!m_positionsFetched) {
        m_positions = fetchPositions(mdb_cursor_txn(m_cursor), m_positionDbi, m_key, m_val);
        m_positionsFetched = true;
    }
    return m_positions;
}

PostingIterator* PostingDB::iter(const QByteArray &term)
{
    Q_ASSERT(!term.isEmpty());
    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::termIter" << mdb_strerror(rc);
        return nullptr;
    }
    MDB_val key, val{0, nullptr};
    key.mv_size = term.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(term.data()));
    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "PostingDB::termIter" << mdb_strerror(rc);
        }
        mdb_cursor_close(cursor);
        return nullptr;
    }
    return new DBTermIterator(m_positionDbi, cursor, term, std::move(val));
}

PostingIterator* PostingDB::prefixIter(const QByteArray &prefix, std::function<bool (const QByteArray&)> validate)
{
    Q_ASSERT(!prefix.isEmpty());

    QVector<PostingIterator*> iterators;
    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_postingDbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "PostingDB::get" << mdb_strerror(rc);
        return nullptr;
    }

    MDB_val key, val{0, nullptr};
    key.mv_size = prefix.size();
    key.mv_data = reinterpret_cast<void*>(const_cast<char*>(prefix.data()));

    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET_RANGE);
    while (rc == 0) {
        QByteArray term = QByteArray::fromRawData(reinterpret_cast<char*>(key.mv_data), key.mv_size);
        if (!term.startsWith(prefix)) {
            break;
        }
        if (validate(term)) {
            iterators.append(iter(term));
            /*QVector<quint64> ids;
            while (rc == 0) {
                ids.append(*reinterpret_cast<quint64*>(val.mv_data));
                rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT_DUP);
            }
            if (rc != MDB_NOTFOUND) {
                break;
            }
            iterators.append(new VectorPostingIterator(ids));*/
        }
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT_NODUP);
    }
    if (rc != 0 && rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "PostingDB::prefixIter" << mdb_strerror(rc);
    }
    mdb_cursor_close(cursor);
    return new OrPostingIterator(iterators);
}

PostingIterator* PostingDB::compIter(const QByteArray &prefix, qlonglong comVal, Comparator com)
{
    int prefixLen = prefix.size();
    auto validate = [prefixLen, comVal, com] (const QByteArray& term) {
        bool ok = false;
        qlonglong val = QByteArray::fromRawData(term.data() + prefixLen, term.size() - prefixLen).toLongLong(&ok);
        return ok && ((com == LessEqual && val <= comVal) || (com == GreaterEqual && val >= comVal));
    };
    return prefixIter(prefix, validate);
}


