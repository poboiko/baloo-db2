/*
   This file is part of the KDE Baloo project.
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "andpostingiterator.h"

using namespace Baloo;

AndPostingIterator::AndPostingIterator(const QVector<PostingIterator*>& iterators)
    : m_iterators(iterators)
    , m_docId(0)
{
    if (m_iterators.contains(nullptr)) {
        qDeleteAll(m_iterators);
        m_iterators.clear();
    }
}

AndPostingIterator::~AndPostingIterator()
{
    qDeleteAll(m_iterators);
}

quint64 AndPostingIterator::docId() const
{
    return m_docId;
}

quint64 AndPostingIterator::next()
{
    if (m_iterators.isEmpty()) {
        m_docId = 0;
        return 0;
    }

    for (int i = 0; i < m_iterators.size(); i++) {
        quint64 curid = m_iterators[i]->next();
        if (curid == 0) {
            m_docId = 0;
            return 0;
        }
        m_docId = qMax(m_docId, curid);
    }

    bool found;
    do {
        found = true;
        for (int i = 0; i < m_iterators.size(); i++) {
            quint64 cur = m_iterators[i]->skipTo(m_docId);
            if (cur == 0) {
                m_docId = 0;
                return 0;
            }
            found &= (m_docId == cur);
            m_docId = qMax(m_docId, cur);
        }
    } while (!found);
    return m_docId;
}

/**
 * Phrase posting iterator
 */
PhraseAndPostingIterator::PhraseAndPostingIterator(const QVector<PostingIterator*>& iterators)
    : AndPostingIterator(iterators)
{
}

quint64 PhraseAndPostingIterator::next()
{
    do {
         m_docId = AndPostingIterator::next();
    } while (m_docId > 0 && !checkIfPositionsMatch());
    return m_docId;
}

QVector<quint32> PhraseAndPostingIterator::positions()
{
    if (m_docId == 0) {
        m_positions.clear();
    }
    return m_positions;
}

bool PhraseAndPostingIterator::checkIfPositionsMatch()
{
    QVector< QVector<uint> > positionList;
    positionList.reserve(m_iterators.size());
    // All the iterators should have the same value
    for (int i = 0; i < m_iterators.size(); i++) {
        auto* iter = m_iterators[i];
        Q_ASSERT(iter->docId() == m_docId);

        QVector<uint> pi = iter->positions();
        for (int j = 0; j < pi.size(); j++) {
            pi[j] -= i;
        }

        positionList << pi;
    }

    // Intersect all these positions
    m_positions = positionList[0];
    for (int l = 1; l < positionList.size(); l++) {
        QVector<uint> newVec = positionList[l];

        int i = 0;
        int j = 0;
        QVector<uint> finalVec;
        while (i < m_positions.size() && j < newVec.size()) {
            if (m_positions[i] == newVec[j]) {
                finalVec << m_positions[i];
                i++;
                j++;
            } else if (m_positions[i] < newVec[j]) {
                i++;
            } else {
                j++;
            }
        }

        m_positions = finalVec;
    }

    return !m_positions.isEmpty();
}
