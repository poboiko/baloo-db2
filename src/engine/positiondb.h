#ifndef BALOO_POSITIONDB_H
#define BALOO_POSITIONDB_H

#include <lmdb.h>
#include "engine_export.h"
#include <QtGlobal>

namespace Baloo {

/**
 * This class allows maps <hash> -> <positions>
 *
 * It is used to store long position lists, that did not fit inside PostingDB
 */
class BALOO_ENGINE_EXPORT PositionDB
{
public:
    static const unsigned char MAX_DELTA = 0xFF;

    PositionDB(MDB_dbi positionDbi, MDB_txn *txn);

    static MDB_dbi create(MDB_txn* txn);
    static MDB_dbi open(MDB_txn* txn);

    /**
     * Implementation of linear probing technique to solve hash collisions:
     * find nearest delta such that hash+delta is empty
     * Returns MAX_DELTA on error
     */
    unsigned char probe(quint64 hash);

    /**
     * Put positions list to DB
     * @p data is assumed to be encoded using PositionCodec
     * Returns error code by LMDB
     */
    int put(quint64 hash, const QByteArray& data);

    /**
     * Remove key @p hash from DB
     * Return LMDB error code
     */
    int del(quint64 hash);

    /**
     * Fetch positions by @p hash
     */
    QVector<quint32> get(quint64 hash);

    /**
     * Return count of entries obtainable by @p hash
     */
    uint count(quint64 hash);

    /**
     * Fetch all data from DB
     */
    QMap<quint64,QVector<quint32>> toTestMap();
private:
    MDB_txn* m_txn;
    MDB_dbi m_dbi;

    friend class DBTermIterator;
};

}

#endif // BALOO_POSITIONDB_H
