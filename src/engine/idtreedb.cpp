/*
   This file is part of the KDE Baloo project.
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "idtreedb.h"
#include "enginedebug.h"
#include "postingiterator.h"

#include <algorithm>

using namespace Baloo;

IdTreeDB::IdTreeDB(MDB_dbi dbi, MDB_txn* txn)
    : m_txn(txn)
    , m_dbi(dbi)
{
    Q_ASSERT(txn != nullptr);
    Q_ASSERT(dbi != 0);
}

MDB_dbi IdTreeDB::create(MDB_txn* txn)
{
    MDB_dbi dbi = 0;
    int rc = mdb_dbi_open(txn, "idtree", MDB_CREATE | MDB_INTEGERKEY | MDB_DUPSORT | MDB_DUPFIXED | MDB_INTEGERDUP, &dbi);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::create" << mdb_strerror(rc);
        return 0;
    }

    return dbi;
}

MDB_dbi IdTreeDB::open(MDB_txn* txn)
{
    MDB_dbi dbi = 0;
    int rc = mdb_dbi_open(txn, "idtree", MDB_INTEGERKEY | MDB_DUPSORT | MDB_DUPFIXED | MDB_INTEGERDUP, &dbi);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::open" << mdb_strerror(rc);
        return 0;
    }

    return dbi;
}

void IdTreeDB::put(quint64 docId, const QVector<quint64> subDocIds)
{
    Q_ASSERT(!subDocIds.isEmpty());
    Q_ASSERT(!subDocIds.contains(0));

    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_dbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::put" << mdb_strerror(rc);
        return;
    }

    MDB_val key;
    key.mv_size = sizeof(quint64);
    key.mv_data = &docId;

    MDB_val val[2];
    val[0].mv_size = sizeof(quint64);
    val[0].mv_data = reinterpret_cast<void*>(const_cast<quint64*>(subDocIds.data()));
    val[1].mv_size = subDocIds.size();
    val[1].mv_data = nullptr;

    rc = mdb_cursor_put(cursor, &key, val, MDB_MULTIPLE);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::put" << mdb_strerror(rc);
    } else if (val[1].mv_size != subDocIds.size()) {
        qCWarning(ENGINE) << "IdTreeDB::put" << val[1].mv_size << "out of" << subDocIds.size() << "ids were written to DB!";
    }
}

void IdTreeDB::add(quint64 docId, quint64 subDocId)
{
    Q_ASSERT(subDocId != 0);

    MDB_val key;
    key.mv_size = sizeof(quint64);
    key.mv_data = &docId;

    MDB_val val;
    val.mv_size = sizeof(quint64);
    val.mv_data = &subDocId;

    int rc = mdb_put(m_txn, m_dbi, &key, &val, 0);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::add" << docId << subDocId << mdb_strerror(rc);
    }
}

QVector<quint64> IdTreeDB::get(quint64 docId)
{
    QVector<quint64> result;
    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_dbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::get" << docId << mdb_strerror(rc);
        return result;
    }
    MDB_val key, val{0, nullptr};
    key.mv_size = sizeof(quint64);
    key.mv_data = &docId;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "IdTreeDB::get" << docId << mdb_strerror(rc);
        }
        mdb_cursor_close(cursor);
        return result;
    }

    result = fetchByCursor(cursor, key, val);
    mdb_cursor_close(cursor);
    return result;
}

void IdTreeDB::del(quint64 docId, quint64 subDocId)
{
    MDB_val key;
    key.mv_size = sizeof(quint64);
    key.mv_data = &docId;
    int rc;
    if (subDocId > 0) {
        MDB_val val;
        val.mv_size = sizeof(quint64);
        val.mv_data = &subDocId;
        rc = mdb_del(m_txn, m_dbi, &key, &val);
    } else {
        rc = mdb_del(m_txn, m_dbi, &key, nullptr);
    }
    if (rc != 0 && rc != MDB_NOTFOUND) {
        qCDebug(ENGINE) << "IdTreeDB::del" << mdb_strerror(rc);
    }
}

int IdTreeDB::count(quint64 docId)
{
    MDB_cursor *cursor;
    int rc = mdb_cursor_open(m_txn, m_dbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::count" << docId << mdb_strerror(rc);
        return 0;
    }
    MDB_val key, val{0, nullptr};
    key.mv_size = sizeof(quint64);
    key.mv_data = &docId;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_SET);
    if (rc) {
        if (rc != MDB_NOTFOUND) {
            qCWarning(ENGINE) << "IdTreeDB::count" << docId << mdb_strerror(rc);
        }
        mdb_cursor_close(cursor);
        return 0;
    }

    size_t count = 0;
    rc = mdb_cursor_count(cursor, &count);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::count" << docId << mdb_strerror(rc);
    }
    mdb_cursor_close(cursor);
    return count;

}

QVector<quint64> IdTreeDB::fetchByCursor(MDB_cursor *cursor, MDB_val &key, MDB_val &val) const
{
    // Keys with single value are not stored in a sub-db, so MDB_*_MULTIPLE won't work on it
    size_t count;
    int rc = mdb_cursor_count(cursor, &count);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::fetchByCursor" << mdb_strerror(rc);
        return {};
    }
    if (count == 1) {
        return { *reinterpret_cast<quint64*>(val.mv_data) };
    }
    QVector<quint64> result;
    rc = mdb_cursor_get(cursor, &key, &val, MDB_GET_MULTIPLE);
    while (rc == 0) {
        std::vector<quint64> vec;
        vec.assign(reinterpret_cast<quint64*>(val.mv_data),
                   reinterpret_cast<quint64*>(reinterpret_cast<char*>(val.mv_data) + val.mv_size));
        result.append(QVector<quint64>::fromStdVector(vec));
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT_MULTIPLE);
    }
    if (rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "IdTreeDB::fetchByCursor" << mdb_strerror(rc);
    }
    return result;
}

//
// Iter
//
class IdTreePostingIterator : public PostingIterator {
public:
    IdTreePostingIterator(const IdTreeDB& db, const QVector<quint64> list)
        : m_db(db), m_pos(-1), m_idList(list) {}

    quint64 docId() const override {
        if (m_pos >= 0 && m_pos < m_resultList.size())
            return m_resultList[m_pos];
        return 0;
    }

    quint64 next() override {
        if (m_resultList.isEmpty() && m_idList.isEmpty()) {
            return 0;
        }

        if (m_resultList.isEmpty()) {
            while (!m_idList.isEmpty()) {
                quint64 id = m_idList.takeLast();
                m_idList << m_db.get(id);
                m_resultList << id;
            }
            std::sort(m_resultList.begin(), m_resultList.end());
            m_pos = 0;
        }
        else {
            if (m_pos < m_resultList.size())
                m_pos++;
            else
                m_resultList.clear();
        }

        if (m_pos < m_resultList.size())
            return m_resultList[m_pos];
        else
            return 0;
    }

private:
    IdTreeDB m_db;
    int m_pos;
    QVector<quint64> m_idList;
    QVector<quint64> m_resultList;
};

PostingIterator* IdTreeDB::iter(quint64 docId)
{
    Q_ASSERT(docId > 0);

    QVector<quint64> list = {docId};
    return new IdTreePostingIterator(*this, list);
}

QMap<quint64, QVector<quint64>> IdTreeDB::toTestMap() const
{
    QMap<quint64, QVector<quint64>> map;
    MDB_cursor* cursor;
    int rc = mdb_cursor_open(m_txn, m_dbi, &cursor);
    if (rc) {
        qCWarning(ENGINE) << "IdTreeDB::toTestMap" << mdb_strerror(rc);
        return map;
    }

    MDB_val key{0, nullptr};
    MDB_val val{0, nullptr};
    rc = mdb_cursor_get(cursor, &key, &val, MDB_FIRST);
    while (rc == 0) {
        const quint64 id = *reinterpret_cast<quint64*>(key.mv_data);
        map.insert(id, fetchByCursor(cursor, key, val));
        rc = mdb_cursor_get(cursor, &key, &val, MDB_NEXT);
    }
    if (rc != MDB_NOTFOUND) {
        qCWarning(ENGINE) << "IdTreeDB::toTestMap" << mdb_strerror(rc);
    }

    mdb_cursor_close(cursor);
    return map;
}
