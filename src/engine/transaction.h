/*
 * This file is part of the KDE Baloo project.
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef BALOO_TRANSACTION_H
#define BALOO_TRANSACTION_H

#include "databasedbis.h"
#include "mtimedb.h"
#include "postingdb.h"
#include "documenttimedb.h"
#include "enginedebug.h"
#include "document.h"
#include "documentoperations.h"

#include <QString>
#include <lmdb.h>

namespace Baloo {

class Database;
class Document;
class PostingIterator;
class EngineQuery;
class DatabaseSize;
class DBState;

class BALOO_ENGINE_EXPORT Transaction
{
public:
    enum TransactionType {
        ReadOnly,
        ReadWrite
    };
    Transaction(const Database& db, TransactionType type);
    Transaction(Database* db, TransactionType type);
    ~Transaction();

    //
    // Getters
    //
    bool hasDocument(quint64 id) const;
    bool inPhaseOne(quint64 id) const;
    bool hasFailed(quint64 id) const;
    QVector<quint64> failedIds(quint64 limit) const;
    QByteArray documentUrl(quint64 id) const;

    /**
     * This method is not cheap, and does not stat the filesystem in order to convert the path
     * \p path into an id.
     */
    quint64 documentId(const QByteArray& path) const;
    QVector<quint64> childrenDocumentId(quint64 parentId) const;
    QByteArray documentData(quint64 id) const;

    DocumentTimeDB::TimeInfo documentTimeInfo(quint64 id) const;

    QVector<quint64> exec(const EngineQuery& query, int limit = -1) const;

    PostingIterator* postingIterator(const EngineQuery& query) const;
    PostingIterator* postingCompIterator(const QByteArray& prefix, qlonglong value, PostingDB::Comparator com) const;
    PostingIterator* mTimeIter(quint32 mtime, MTimeDB::Comparator com) const;
    PostingIterator* mTimeRangeIter(quint32 beginTime, quint32 endTime) const;
    PostingIterator* docUrlIter(quint64 id) const;

    QVector<quint64> fetchPhaseOneIds(int size) const;
    uint phaseOneSize() const;
    uint size() const;

    QVector<QByteArray> fetchTermsStartingWith(const QByteArray& term) const;
    //
    // Statistics
    //
    /**
     * Total amount of documents in DB
     */
    uint count();
    /**
     * Total amount of documents containing @p term
     */
    uint count(const QByteArray& term);
    /**
     * Total amount of encounters of @p term in document @p docId
     */
    uint count(const QByteArray& term, quint64 docId);

    //
    // Introspecing document data
    //
    QVector<QByteArray> documentTerms(quint64 docId) const;
    QVector<QByteArray> documentFileNameTerms(quint64 docId) const;
    QVector<QByteArray> documentXattrTerms(quint64 docId) const;

    DatabaseSize dbSize();

    //
    // Transaction handling
    //
    void commit();
    void abort();

    //
    // Write Methods
    //
    void addDocument(const Document& doc);
    void removeDocument(quint64 id);
    void replaceDocument(const Document& doc, DocumentOperations operations);

    /**
     * Goes through every document in the database, and remove the ones for which \p shouldDelete
     * returns false. It starts searching from \p parentId, which can be 0 to search
     * through everything.
     *
     * \arg shouldDelete takes a quint64 as a parameter
     *
     * This function should typically be called when there are no other ReadTransaction in process
     * as that would otherwise balloon the size of the database.
     */
    void removeRecursively(quint64 id,
                           std::function<bool (quint64)> shouldDelete = [](quint64) -> bool { return true; });

    void addFailed(quint64 id);
    void setPhaseOne(quint64 id);
    void removePhaseOne(quint64 id);

    // Debugging
    QMap<QByteArray, PostingMap> postingMap();
    void checkFsTree();
    void checkTermsDbinPostingDb();
    void checkPostingDbinTermsDb();

private:
    Transaction(const Transaction& rhs) = delete;

    QVector<QByteArray> addTerms(quint64 id, const QMap<QByteArray, PositionsList>& terms);
    void removeTerms(quint64 id, const QVector<QByteArray>& terms);
    QVector<QByteArray> replaceTerms(quint64 id, const QVector<QByteArray>& prevTerms,
                                     const QMap<QByteArray, PositionsList>& terms);

    const DatabaseDbis& m_dbis;
    MDB_txn *m_txn = nullptr;
    MDB_env *m_env = nullptr;
    TransactionType m_type;

    friend class DatabaseSanitizerImpl;
    friend class DBState; // for testing
};
}

#endif // BALOO_TRANSACTION_H
