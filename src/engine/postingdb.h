#ifndef BALOO_POSTINGDB_H
#define BALOO_POSTINGDB_H

#include "postingiterator.h"

#include <lmdb.h>
#include <QDebug>

namespace Baloo {

/**
 * Single Posting/PositionDB entry.
 *
 * For now consists only of positions
 */

typedef QVector<quint32> PositionsList;
typedef QMap<quint64, PositionsList> PostingMap;

/**
 * This class allows matching <term> -> <docId, TermData>
 *
 * Under the hood, this is done by means of the following DB structure:
 * - PostingDB keys are <term>
 * - PostingDB values can be one of following types:
 *   - <docId>. This corresponds to empty <positions>
 *   - <docId,positions>. This is possible only if size of value
 * is less then 511 bytes (maxkeysize), restriction of MDB_DUPSORT
     - <docid,delta>. If size of <positions> is larger than maxkeysize,
 * we store <positions> inside PositionDB.
 *
 * In PositionDB, keys are hash(term + docId) pair (so that we don't need to store it),
 * and values are <positions>. In order to deal with hash collisions, we use linear
 * probing technique (i.e. if hash is used, we try hash+1, hash+2, ..., and then store
 * corresponding delta inside PostingDB)
 */

class BALOO_ENGINE_EXPORT PostingDB
{
public:
    PostingDB(MDB_dbi postingDbi, MDB_dbi positionDbi, MDB_txn *txn);

    static MDB_dbi create(MDB_txn* txn);
    static MDB_dbi open(MDB_txn* txn);

    /**
     * Add new term information to database. Overwrite if (term, docId) pair already exists
     */
    void put(const QByteArray& term, quint64 docId, const PositionsList& positions = {});

    /**
     * Get list of all documents which contain @p term
     */
    QVector<quint64> get(const QByteArray& term);

    /**
     * Get list of all positions of @p term inside document @p docId
     */
    PositionsList get(const QByteArray& term, quint64 docId);

    /**
     * Remove (term, docId) pair from database
     */
    void del(const QByteArray& term, quint64 docId);

    /**
     * Total amount of documents that countan @p term
     */
    uint count(const QByteArray& term);

    /**
     * Term frequency: how many times @p term is encountered in document @p docId
     */
    uint count(const QByteArray& term, quint64 docId);

    /**
     * Fetch terms starting with @p prefix
     */
    QVector<QByteArray> fetchTermsStartingWith(const QByteArray& prefix);

    /**
     * Returns iterator for @p term, which also provides information about positions
     */
    PostingIterator* iter(const QByteArray& term);

    /**
     * Returns an iterator for all terms that start with @p prefix
     * and for which validate() function returns true
     */
    PostingIterator* prefixIter(const QByteArray& prefix,
                                std::function<bool (const QByteArray&)> validate = [](const QByteArray&) -> bool { return true; });

    /**
     * Special case of prefixIter, for which validate() function
     * strips @p prefix from term, and then compares the value
     * with @p comVal
     */
    enum Comparator {
        LessEqual,
        GreaterEqual
    };
    PostingIterator* compIter(const QByteArray& prefix, qlonglong comVal, Comparator com);

    /**
     * Returns all data in both DBs
     */
    QMap<QByteArray, PostingMap> toTestMap();
private:
    MDB_txn* m_txn;
    MDB_dbi m_postingDbi, m_positionDbi;

    int m_maxkeysize;

    friend class DBTermIterator;
};

}

#endif // BALOO_POSTINGDB_H
