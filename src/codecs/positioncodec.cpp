/*
 * This file is part of the KDE Baloo Project
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "positioncodec.h"
#include "coding.h"

using namespace Baloo;

QByteArray PositionCodec::encode(const QVector<quint32> &list)
{
    QByteArray data, temp;
    putDifferentialVarInt32(temp, &data, list);
    return data;
}

QVector<quint32> PositionCodec::decode(const QByteArray& arr)

{
    QVector<quint32> result;
    char* data = const_cast<char*>(arr.data());
    char* end = data + arr.size();
    data = getDifferentialVarInt32(data, end, &result);
    return result;
}

uint PositionCodec::size(const QByteArray &arr)
{
    quint32 value;
    getVarint32Ptr(const_cast<char*>(arr.data()), const_cast<char*>(arr.data() + arr.size()), &value);
    return value;
}
