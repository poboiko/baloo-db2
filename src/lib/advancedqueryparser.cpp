/*
 * Copyright (C) 2014-2015  Vishesh Handa <vhanda@kde.org>
 * Copyright (C) 2014  Denis Steckelmacher <steckdenis@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "advancedqueryparser.h"

#include <QStringList>
#include <QStack>
#include <QDate>

using namespace Baloo;

class Token
{
public:
    Token()
        : m_token()
        , m_inQuotes(false)
    {
    }

    Token(const QString& token)
        : m_token(token)
        , m_inQuotes(false)
    {
    }

    void append(QChar c)
    {
        m_token.append(c);
    }

    QString value() const
    {
        return m_token;
    }

    bool isEmpty() const
    {
        return m_token.isEmpty();
    }

    QVariant toVariant() const
    {
        bool okay = false;
        int intValue = m_token.toInt(&okay);
        if (okay) {
            return QVariant(intValue);
        }

        QDate date = QDate::fromString(m_token, Qt::ISODate);
        if (date.isValid() && !date.isNull()) {
            QDateTime dateTime = QDateTime::fromString(m_token, Qt::ISODate);
            if (dateTime.isValid() && !dateTime.isNull()) {
                return dateTime;
            }
            return date;
        }

        return QVariant(m_token);
    }

    Term::Comparator toComp() const
    {
        if (m_token == QLatin1String(":"))
            return Term::Contains;
        else if (m_token == QLatin1String("="))
            return Term::Equal;
        else if (m_token == QLatin1String(">="))
            return Term::GreaterEqual;
        else if (m_token == QLatin1String(">"))
            return Term::Greater;
        else if (m_token == QLatin1String("<="))
            return Term::LessEqual;
        else if (m_token == QLatin1String("<"))
            return Term::Less;
        else
            return Term::Auto;
    }

    Term::Operation toOp() const
    {
        if (m_token == QLatin1String("AND"))
            return Term::And;
        else if (m_token == QLatin1String("OR"))
            return Term::Or;
        else
            return Term::None;
    }

    bool inQuotes() const
    {
        return m_inQuotes;
    }

    void setInQuotes(bool inQuotes)
    {
        m_inQuotes = inQuotes;
    }
private:
    QString m_token;
    bool m_inQuotes;
};

AdvancedQueryParser::AdvancedQueryParser()
{
}

static QList<Token> lex(const QString& text)
{
    QList<Token> tokenList;
    Token token;

    for (int i = 0, end = text.size(); i != end; ++i) {
        QChar c = text.at(i);

        if (c == QLatin1Char('"')) {
            if (token.inQuotes()) {
                // End of quoted token
                if (!token.isEmpty()) {
                    tokenList.append(token);
                }
                token = Token();
            } else {
                if (!token.isEmpty()) {
                    // Quotes are somewhere in the middle of text, i.e. [... don"t ...]
                    // We treat it as if there was a space, i.e. [...don "t...]
                    tokenList.append(token);
                    token = Token();
                }
                token.setInQuotes(true);
            }
        } else if (token.inQuotes()) {
            // Don't do any processing in double-quoted tokens
            token.append(c);
        } else if (c.isSpace()) {
            // Spaces end tokens
            if (!token.isEmpty()) {
                tokenList.append(token);
                token = Token();
            }
        } else if (c == QLatin1Char('(') || c == QLatin1Char(')')) {
            // Parentheses end tokens, and are tokens by themselves
            if (!token.isEmpty()) {
                tokenList.append(token);
                token = Token();
            }
            tokenList.append(Token(QString(c)));
        } else if (c == QLatin1Char('>') || c == QLatin1Char('<') || c == QLatin1Char(':') || c == QLatin1Char('=')) {
            // Operators end tokens
            if (!token.isEmpty()) {
                tokenList.append(token);
                token = Token();
            }
            // accept '=' after any of the above
            if (text.at(i + 1) == QLatin1Char('=')) {
                tokenList.append(Token(text.mid(i, 2)));
                i++;
            } else {
                tokenList.append(Token(QString(c)));
            }
        } else {
            // Simply extend the current token
            token.append(c);
        }
    }

    if (!token.isEmpty()) {
        // If quotes didn't close, then it wasn't actually a phrase
        token.setInQuotes(false);
        tokenList.append(token);
    }

    return tokenList;
}

static void addTermToStack(QStack<Term>& stack, const Term& termInConstruction, Term::Operation op)
{
    Term &tos = stack.top();

    if (tos.isEmpty()) {
        // Empty top of stack, just assign termInConstruction to it
        tos = termInConstruction;
        return;
    }

    tos = Term(tos, op, termInConstruction);
}

Term AdvancedQueryParser::parse(const QString& text)
{
    // The parser does not do any look-ahead but has to store some state
    QStack<Term> stack;
    QStack<Term::Operation> ops;
    Term termInConstruction;
    bool valueExpected = false;

    stack.push(Term());
    ops.push(Term::And);

    // Lex the input string
    const QList<Token> tokens = lex(text);
    for (const Token &token : tokens) {
        // If a key and an operator have been parsed, now is time for a value
        if (valueExpected) {
            // When the parser encounters a literal, it puts it in the value of
            // termInConstruction so that "foo bar baz" is parsed as expected.
            auto property = termInConstruction.value().toString();
            if (property.isEmpty()) {
                qDebug() << "Binary operator without first argument encountered:" << text;
                return Term();
            }
            termInConstruction.setProperty(property);

            QVariant value = token.toVariant();
            if (token.inQuotes()
                    || (value.type() != QVariant::String && termInConstruction.comparator() == Term::Contains)) {
                termInConstruction.setComparator(Term::Equal);
            }

            termInConstruction.setValue(value);
            valueExpected = false;
            continue;
        }

        // Handle the logic operators
        Term::Operation op = token.toOp();
        if (op != Term::None) {
            if (!termInConstruction.isEmpty()) {
                addTermToStack(stack, termInConstruction, ops.top());
                termInConstruction = Term();
            }
            ops.top() = op;
            continue;
        }

        // Handle braces
        switch (token.value().at(0).toLatin1()) {
        case '(': {
            if (!termInConstruction.isEmpty()) {
                addTermToStack(stack, termInConstruction, ops.top());
                ops.top() = Term::And;
            }

            stack.push(Term());
            ops.push(Term::And);
            termInConstruction = Term();

            continue;
        }
        case ')': {
            // Prevent a stack underflow if the user writes "a b ))))"
            if (stack.size() > 1) {
                // Don't forget the term just before the closing brace
                if (termInConstruction.value().isValid()) {
                    addTermToStack(stack, termInConstruction, ops.top());
                }

                // stack.pop() is the term that has just been closed. Append
                // it to the term just above it.
                ops.pop();
                addTermToStack(stack, stack.pop(), ops.top());
                ops.top() = Term::And;
                termInConstruction = Term();
            }

            continue;
        }
        default:
            break;
        }

        // Handle comparators
        Term::Comparator comparator = token.toComp();
        if (comparator != Term::Auto) {
            // Set the comparator of the term in construction and expect a value
            termInConstruction.setComparator(comparator);
            valueExpected = true;
        } else {
            // A new term will be started, so termInConstruction has to be appended
            // to the top-level subterm list.
            if (!termInConstruction.isEmpty()) {
                addTermToStack(stack, termInConstruction, ops.top());
                ops.top() = Term::And;
            }

            termInConstruction = Term(QString(), token.value(), token.inQuotes() ? Term::Equal : Term::Auto);
        }
    }

    if (termInConstruction.value().isValid()) {
        addTermToStack(stack, termInConstruction, ops.top());
    }

    return stack.top();
}

