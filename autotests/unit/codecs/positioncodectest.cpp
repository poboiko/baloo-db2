/*
   This file is part of the KDE Baloo project.
   Copyright (C) 2016 Christian Ehrlicher <ch.ehrlicher@gmx.de>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QCryptographicHash>
#include <QtTest>
#include "positioncodec.h"

using namespace Baloo;

class PositionCodecTest : public QObject
{
Q_OBJECT
public:
    PositionCodecTest() = default;
    ~PositionCodecTest() = default;
private Q_SLOTS:
    void initTestCase();
    void checkEncodeOutput();
    void checkEncodeOutput2();
    void checkEncodeOutput3();
private:
    QVector<quint32> m_data;
    QVector<quint32> m_data2;
    QVector<quint32> m_data3;
};

QTEST_MAIN ( PositionCodecTest )

void PositionCodecTest::initTestCase()
{
    m_data.clear();
    m_data.reserve(3000);
    for (int j = 0; j < 3000; j++)
        m_data.append((j + 1) * 2);

    m_data2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    m_data3.clear();
    m_data3.reserve(30000); // > 2^14 -> 3 byte VarInt32
    for (int j = 0; j < 30000; j++)
        m_data3.append((j + 1) * 200); // increment 200 -> 2 byte DiffVarInt32
}

void PositionCodecTest::checkEncodeOutput()
{
    const QByteArray ba = PositionCodec::encode(m_data);
    QCOMPARE(ba.size(), 3002);
    const QByteArray md5 = QCryptographicHash::hash(ba, QCryptographicHash::Md5).toHex();
    QCOMPARE(md5, QByteArray("0124ece50eee787e2b41d23d6d30d4c8"));
    // and now decode the whole stuff
    QVector<quint32> decodedData = PositionCodec::decode(ba);
    QCOMPARE(m_data, decodedData);
}

void PositionCodecTest::checkEncodeOutput2()
{
    const QByteArray ba = PositionCodec::encode(m_data2);
    QCOMPARE(ba.size(), 11); // DocId, VarInt32 len, DiffVarInt position
    const QByteArray md5 = QCryptographicHash::hash(ba, QCryptographicHash::Md5).toHex();
    QCOMPARE(md5, QByteArray("c6c43961661af714f3afb35f0701f536"));
    // and now decode the whole stuff
    QVector<quint32> decodedData = PositionCodec::decode(ba);
    QCOMPARE(m_data2, decodedData);
}

void PositionCodecTest::checkEncodeOutput3()
{
    const QByteArray ba = PositionCodec::encode(m_data3);
    QCOMPARE(ba.size(), 3 + (2 * 30000)); // VarInt32 len, DiffVarInt position
    const QByteArray md5 = QCryptographicHash::hash(ba, QCryptographicHash::Md5).toHex();
    QCOMPARE(md5, QByteArray("29d20bc4d2f54d73eed4706c5bcc9859"));
    // and now decode the whole stuff
    QVector<quint32> decodedData = PositionCodec::decode(ba);
    QCOMPARE(m_data3, decodedData);
}

#include "positioncodectest.moc"
