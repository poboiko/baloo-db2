/*
   This file is part of the KDE Baloo project.
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "phraseanditerator.h"
#include "postingdb.h"
#include <QTest>

using namespace Baloo;

class TestPostingIterator : public PostingIterator
{
public:
    TestPostingIterator(const PostingMap::const_iterator& it,
                        const PostingMap::const_iterator& end)
        : m_it(it), m_end(end), m_first(true)
    {
    }

    quint64 docId() const override
    {
        if (m_first || (m_it == m_end)) {
            return 0;
        }
        return m_it.key();
    }

    quint64 next() override
    {
        if (m_first) {
            m_first = false;
        } else
            m_it++;
        return docId();
    }

    QVector<quint32> positions() override
    {
        if (m_first || (m_it == m_end)) {
            return {};
        }
        return m_it.value();
    }
private:
    PostingMap::const_iterator m_it, m_end;
    bool m_first;
};

class PhraseAndIteratorTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void test();
    void testNullIterators();
};

void PhraseAndIteratorTest::test()
{
    PostingMap map1 = {{2, {{5, 9}}},
                       {4, {{4, 2}}}};
    PostingMap map2 = {{2, {{6, 7}}},
                       {4, {{6, 2}}},
                       {7, {{1, 4, 2}}}};

    TestPostingIterator* it1 = new TestPostingIterator(map1.cbegin(), map1.cend());
    TestPostingIterator* it2 = new TestPostingIterator(map2.cbegin(), map2.cend());
    PhraseAndIterator it({it1, it2});

    QCOMPARE(it.docId(), static_cast<quint64>(0));

    // The Query is "term1 term2". term1 must appear one position before term2
    QVector<quint64> result = {2};
    for (quint64 val : result) {
        QCOMPARE(it.next(), static_cast<quint64>(val));
        QCOMPARE(it.docId(), static_cast<quint64>(val));
    }
    QCOMPARE(it.next(), static_cast<quint64>(0));
    QCOMPARE(it.docId(), static_cast<quint64>(0));
}

void PhraseAndIteratorTest::testNullIterators()
{
    // Term 1
    PostingMap map1 = {{2, {{5, 9}}}};
    PostingMap map2 = {{2, {{6, 7}}}};
    TestPostingIterator* it1 = new TestPostingIterator(map1.cbegin(), map1.cend());
    TestPostingIterator* it2 = new TestPostingIterator(map2.cbegin(), map2.cend());
    PhraseAndIterator it({it1, nullptr, it2});
    QCOMPARE(it.docId(), static_cast<quint64>(0));
    QCOMPARE(it.next(), static_cast<quint64>(0));
    QCOMPARE(it.docId(), static_cast<quint64>(0));
}

QTEST_MAIN(PhraseAndIteratorTest)

#include "phraseanditeratortest.moc"
