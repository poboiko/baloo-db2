/*
   This file is part of the KDE Baloo project.
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "postingdb.h"
#include "positiondb.h"
#include "dbtest.h"

using namespace Baloo;

class PostingPositionDBTest : public DBTest
{
    Q_OBJECT
private:
    inline void putList(PostingDB& db, const QByteArray& term, const QVector<quint64>& docIds)
    {
        for (quint64 id : docIds) {
            db.put(term, id);
        }
    }

    inline void testIterator(PostingIterator* it, QVector<quint64> result)
    {
        QVERIFY(it);
        for (quint64 id : result) {
            QCOMPARE(it->next(), id);
            QCOMPARE(it->docId(), id);
        }
        QCOMPARE(it->next(), 0);
        delete it;
    }

private Q_SLOTS:
    void testPosting() {
        PostingDB db(PostingDB::create(m_txn), PositionDB::create(m_txn), m_txn);
        putList(db, "test", {1, 2, 3});
        db.put("teso", 3);
        db.put("nottest", 4);

        // Testing for existing term
        QCOMPARE(db.get("test"), QVector<quint64>({1, 2, 3}));
        // Testing term with single value (no duplicates)
        QCOMPARE(db.get("teso"), QVector<quint64>({3}));
        // Testing for non-existing term
        QCOMPARE(db.get("testo"), {});
        // Testing fetchTermsStartingWith
        QCOMPARE(db.fetchTermsStartingWith("tes"), QVector<QByteArray>({ "teso", "test" }));
        // Testing term removal
        db.del("test", 1);
        QCOMPARE(db.get("test"), QVector<quint64>({2, 3}));
    }

    void testSmallPositionData() {
        PostingDB db(PostingDB::create(m_txn), PositionDB::create(m_txn), m_txn);
        db.put("test", 2, {1});
        db.put("teso", 2, {1, 2, 3});
        db.put("fire", 1);

        // Testing existing term, but with non-existing docId
        QCOMPARE(db.get("test", 1), {});
        // Testing smallest possible position info
        QCOMPARE(db.get("test", 2), QVector<quint32>({ 1 }));
        // Testing relatively small position info
        QCOMPARE(db.get("teso", 2), QVector<quint32>({ 1, 2, 3 }));
        // Testing term without positions
        QCOMPARE(db.get("fire", 1), {});
        // Testing non-existing term
        QCOMPARE(db.get("fore", 2), {});
        // Testing count
        QCOMPARE(db.count("test", 2), 1);
        QCOMPARE(db.count("test", 1), 0);
        QCOMPARE(db.count("teso", 2), 3);
        // Testing overwrite
        db.put("test", 1, {1, 2, 3});
        QCOMPARE(db.get("test", 1), QVector<quint32>({1, 2, 3}));
        // Testing remove
        db.put("test", 1);
        QCOMPARE(db.get("test", 1), {});
    }

    void testLargePositionData() {
        PostingDB db(PostingDB::create(m_txn), PositionDB::create(m_txn), m_txn);

        QVector<quint32> positions1;
        positions1.reserve(512);
        for (int i = 0; i < 512; i++)
            positions1.append(i*257);
        QVector<quint32> positions2;
        positions2.reserve(511);
        for (int i = 0; i < 511; i++)
            positions2.append(i*267);

        // Adding some junk
        db.put("test", 2);
        db.put("teso", 1);

        // Testing write large positions list
        db.put("test", 1, positions1);
        QCOMPARE(db.get("test", 1), positions1);

        // Testing count
        QCOMPARE(db.count("test", 1), positions1.size());

        // Testing overwrite large positions list with small one
        db.put("test", 1, {14, 15});
        QCOMPARE(db.get("test", 1), QVector<quint32>({14, 15}));

        // Testing overwrite of small list with large one (back)
        db.put("test", 1, positions1);
        QCOMPARE(db.get("test", 1), positions1);

        // Testing overwrite of large list with another large list
        db.put("test", 1, positions2);
        QCOMPARE(db.get("test", 1), positions2);

        // Testing removal of large positions list
        db.put("test", 1);
        QCOMPARE(db.get("test", 1), {});
    }

    void testTermIter() {
        PostingDB db(PostingDB::create(m_txn), PositionDB::create(m_txn), m_txn);

        QVector<quint32> positions1 = {1, 2, 4};
        QVector<quint32> positions2 = {8, 16, 32};
        putList(db, "test", {1, 15});
        db.put("test", 5, { positions1 });
        db.put("test", 20, { positions2 });

        // Testing invalid iterator
        QVERIFY(!db.iter("testo"));
        // Testing valid iterator
        PostingIterator* it = db.iter("test");
        QVERIFY(it);
        QCOMPARE(it->next(), 1);
        QCOMPARE(it->docId(), 1);
        QCOMPARE(it->positions(), {});
        QCOMPARE(it->next(), 5);
        QCOMPARE(it->docId(), 5);
        QCOMPARE(it->positions(), positions1);
        QCOMPARE(it->next(), 15);
        QCOMPARE(it->docId(), 15);
        QCOMPARE(it->positions(), {});
        QCOMPARE(it->next(), 20);
        QCOMPARE(it->docId(), 20);
        QCOMPARE(it->positions(), positions2);
        // Finished
        QCOMPARE(it->next(), 0);
        // Testing skipTo
        PostingIterator* it2 = db.iter("test");
        QCOMPARE(it2->skipTo(14), 15);
        QCOMPARE(it2->docId(), 15);
        QCOMPARE(it2->skipTo(25), 0);
        QCOMPARE(it2->docId(), 0);
        delete it;
    }

    void testPrefixIter() {
        PostingDB db(PostingDB::create(m_txn), PositionDB::create(m_txn), m_txn);

        putList(db, "abc", {1, 4, 5, 9, 11});
        putList(db, "fir", {1, 3, 5});
        putList(db, "fire", {1, 8, 9});
        putList(db, "fore", {2, 3, 5});

        PostingIterator* it = db.prefixIter("fir");
        testIterator(it, {1, 3, 5, 8, 9});
    }

    void testCompIter()
    {
        PostingDB db(PostingDB::create(m_txn), PositionDB::create(m_txn), m_txn);

        putList(db, "abc", {1, 4, 5, 9, 11});
        putList(db, "R1", {1, 3, 5, 7});
        putList(db, "R2", {1, 8});
        putList(db, "R3", {2, 3, 5});
        putList(db, "R10", {10, 12});

        // X20- is the internal encoding for KFileMetaData::Property::LineCount
        putList(db, "X20-90", {1, 2});
        putList(db, "X20-1000", {10, 11});

        testIterator(db.compIter("R", 2, PostingDB::GreaterEqual),
                    {1, 2, 3, 5, 8, 10, 12 });

        testIterator(db.compIter("R", 2, PostingDB::LessEqual),
                    {1, 3, 5, 7, 8});

        testIterator(db.compIter("R", 2, PostingDB::LessEqual),
                    {1, 3, 5, 7, 8});

        testIterator(db.compIter("R", 10, PostingDB::GreaterEqual),
                    {10, 12});

        testIterator(db.compIter("X20-", 80, PostingDB::GreaterEqual),
                    {1, 2, 10, 11});

        testIterator(db.compIter("X20-", 100, PostingDB::GreaterEqual),
                    {10, 11});
    }

};



QTEST_MAIN(PostingPositionDBTest)

#include "postingpositiondbtest.moc"
