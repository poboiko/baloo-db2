/*
   This file is part of the KDE Baloo project.
 * Copyright (C) 2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "positioncodec.h"

#include <QTest>
#include <QTemporaryDir>

using namespace Baloo;

class PositionCodecBenchmark : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    // data 1 - small number of documents, each with many positions
    void benchEncodeData1();
    void benchDecodeData1();
    // data 2 - large number of documents, few positions (10) each
    void benchEncodeData2();
    void benchDecodeData2();
    // data 3 - small number of documents, many positions with large increment
    void benchEncodeData3();
    void benchDecodeData3();
private:
    QVector<quint32> m_benchmarkData1;
    QVector<quint32> m_benchmarkData2;
    QVector<quint32> m_benchmarkData3;
};

void PositionCodecBenchmark::initTestCase()
{
    /*
     * Same dataset as in autotests/unit/codecs/positioncodectest.cpp
     * Correctness of encoding/decoding is checked there.
     */
    m_benchmarkData1.clear();
    m_benchmarkData1.reserve(3000);
    for (int j = 0; j < 3000; j++)
        m_benchmarkData1.append((j + 1) * 2);

    m_benchmarkData2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    m_benchmarkData3.clear();
    m_benchmarkData3.reserve(30000); // > 2^14 -> 3 byte VarInt32
    for (int j = 0; j < 30000; j++)
        m_benchmarkData3.append((j + 1) * 200); // increment 200 -> 2 byte DiffVarInt32
}

void PositionCodecBenchmark::benchEncodeData1()
{
    QBENCHMARK { PositionCodec::encode(m_benchmarkData1); }
}

void PositionCodecBenchmark::benchDecodeData1()
{
    const QByteArray ba = PositionCodec::encode(m_benchmarkData1);
    QBENCHMARK { PositionCodec::decode(ba); }
}

void PositionCodecBenchmark::benchEncodeData2()
{
    QBENCHMARK { PositionCodec::encode(m_benchmarkData2); }
}

void PositionCodecBenchmark::benchDecodeData2()
{
    const QByteArray ba = PositionCodec::encode(m_benchmarkData2);
    QBENCHMARK { PositionCodec::decode(ba); }
}

void PositionCodecBenchmark::benchEncodeData3()
{
    QBENCHMARK { PositionCodec::encode(m_benchmarkData3); }
}

void PositionCodecBenchmark::benchDecodeData3()
{
    const QByteArray ba = PositionCodec::encode(m_benchmarkData3);
    QBENCHMARK { PositionCodec::decode(ba); }
}

QTEST_MAIN(PositionCodecBenchmark)

#include "positioncodecbenchmark.moc"
